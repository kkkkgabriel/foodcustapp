import requests
import datetime
import random
import string

class session():
	def __init__(self):
		self.customerid =  ""
		self.vendorid = ""
		self.foodid = ""
		self.comments = ""
		self.orderid = ""
		print("session created!")

	def start_session(self, userid):
		self.customerid = userid

	def add_vendor_to_order(self, vendorid):
		# print("vendor "+vendorid+" added")
		self.vendorid = vendorid

	def get_vendorid_from_order(self):
		return self.vendorid

	def add_food_to_order(self, foodid):
		self.foodid = foodid

	def add_comments_to_order(self, comments):
		self.comments = comments

	def add_orderid_to_order(self, orderid):
		self.orderid = orderid

	def check_order(self):
		r = 1
		if self.foodid == "":
			r = 0;
		if self.vendorid == "":
			r = 0;
		if self.customerid == "":
			r = 0;
		return r

	def get_orderid(self):
		return self.orderid

	def kill(self):
		self.__init__()

	def place_order(self):
		if self.check_order():
			vendorid = self.vendorid
			foodid = self.foodid
			comments = self.comments
			customerid = self.customerid
			date_of_order = str(datetime.datetime.now().strftime("%d-%m-%Y"))
			time_of_order = str(datetime.datetime.now().strftime("%H:%M:%S"))
			orderid = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))
			params = '{"date_of_order":"'+date_of_order+'", "time_of_order":"'+time_of_order+'", "time_of_ready":"", "foodid":"'+foodid+'", "vendorid":"'+vendorid+'","comments": "'+comments+'","customerid":"'+customerid+'", "status": "Pending" }'
			# params = '{"date_of_order":"12-12-2018", "time_of_order":"17:36:31", "time_of_ready":"", "foodid":"jad93","vendorid":"chickenrice","comments":"sunny day today","customerid":"1003532", "status": "Now Preparing" }'
			r = requests.put("https://nbcb-8f230.firebaseio.com/foodapp/orders/{}.json".format(orderid), data=params)
			print(r.status_code, r.reason)

			self.orderid = orderid
		else:
			print("insufficient order details")

	def get_values(self):
		print(self)
		print(dir(self))
		print(self.vendorid)
		print(self.foodid)
		print(self.comments)
		print(self.customerid)