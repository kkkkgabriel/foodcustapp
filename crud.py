import requests
import datetime

def login(user, pw):
	r = requests.get("https://nbcb-8f230.firebaseio.com/foodapp/customers/{}.json?print=pretty".format(user))
	data = r.json()
	try:
		if data['password'] == pw:
			return 1
		else:
			return 0
	except(KeyError):
		return 0

def check_orders(user):
	r = requests.get("https://nbcb-8f230.firebaseio.com/foodapp/orders.json?orderBy=%22customerid%22&equalTo=%22{}%22&print=pretty".format(user))
	data = r.json()
	order = None
	orderid = None
	for k, v in data.items():
		if v['status'] != "Completed":
			order = v
			orderid = k
			break
	return orderid, order

def get_menu_list():
	r = requests.get("https://nbcb-8f230.firebaseio.com/foodapp/vendors.json?print=pretty")
	data = r.json()
	norders = {}
	for key in data.keys():
		r2 = requests.get("https://nbcb-8f230.firebaseio.com/foodapp/orders.json?orderBy=%22vendorid%22&equalTo=%22{}%22&print=pretty".format(key))
		x = 0
		for k,v in r2.json().items():
			if v['status'] == "Pending" or v['status'] == "Now Preparing":
				x += 1
		norders[key] = x
	listviewdata = []
	for k in data.keys():
		s = "{:<15}{} persons in queue".format(k, norders[k])
		listviewdata.append(s)
	return listviewdata

def get_food_list(vendorid):
	r = requests.get("https://nbcb-8f230.firebaseio.com/foodapp/food.json?orderBy=%22vendorid%22&equalTo=%22{}%22&print=pretty".format(vendorid))
	data = r.json()
	# print(r.status_code, r.reason)
	# print(data)
	foodnames = []
	for d in data.values():
		price_str = "{:.2f}".format(d['price'])
		s = '{:<30}{}'.format(d['name'], "$"+price_str)
		foodnames.append(s)
	return foodnames

def get_order_status_list(vendorid):
	r = requests.get("https://nbcb-8f230.firebaseio.com/foodapp/orders.json?orderBy=%22vendorid%22&equalTo=%22{}%22&print=pretty".format(vendorid))
	data = r.json()
	ready = [] # To filter orders that have not been into ready and late
	late = [] # To filter orders that have not been into ready and late
	pending_and_preparing = [] # To filter orders that have not been into pending and now preparing
	print(data)
	for k,v in data.items():
		if v['status'] == "Ready":
			ready.append(k)
		if v['status'] == "Late":
			late.append(k)
		if v['status'] == "Now Preparing" or v['status'] == "Pending":
			pending_and_preparing.append(k)
	return pending_and_preparing, ready, late