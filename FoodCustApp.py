from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button 
from kivy.uix.label import Label
from kivy.app import App
from kivy.uix.textinput import TextInput
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
import requests
from kivy.uix.listview import ListItemButton, ListItemLabel
from session import session
from kivy.clock import Clock
import crud

class loginpage(Screen):
		
	def login(self, *_):
		password = self.pw.text
		user = self.user.text
		if crud.login(user, password):
			# print("login successful!")
			App.get_running_app().sess.start_session(user)
			orderid, order = crud.check_orders(user)
			if orderid != None:
				App.get_running_app().sess.start_session(user)
				App.get_running_app().sess.add_vendor_to_order(order['vendorid'])
				App.get_running_app().sess.add_comments_to_order(order['comments'])
				App.get_running_app().sess.add_food_to_order(order['foodid'])
				App.get_running_app().sess.add_orderid_to_order(orderid)
				self.manager.current = "order_status"
			else:
				self.manager.current = "menu"
		else:
			# print("invalid user")
			self.error()

	def error(self):
		self.errorlabel.text = 'Incorrect Username or Password'
			
### menu screen  ###
class menuListButton(ListItemButton):
	def __init__(self, **kwargs):
		super(menuListButton, self).__init__(**kwargs)
		self.height = "70"

	def on_press(self):
		vendor = self.text.split("  ")[0]
		App.get_running_app().sess.add_vendor_to_order(vendor)
		# sess.get_values()
		obj = self.parent.parent.parent.parent.parent
		obj.manager.current = "foodlist"

class MenuScreen(Screen):

	def logout(self, *_):
		App.get_running_app().sess.kill()
		self.manager.transition.direction = 'left'
		self.manager.current = 'login'


	def on_pre_enter(self):
		self.get_values()
		print("entering menuscreen")

	def get_values(self, *_):
		listviewdata = crud.get_menu_list()
		self.menu_list_view.adapter.data = listviewdata
		self.menu_list_view._trigger_reset_populate()
			

### foodlist screen ###
class foodListButton(ListItemButton):

	def __init__(self, **kwargs):
		super(foodListButton, self).__init__(**kwargs)
		self.height = "70"

	def on_press(self):
		App.get_running_app().sess.add_food_to_order(self.text.split("   ")[0])
		# sess.get_values()
		obj = self.parent.parent.parent.parent.parent
		obj.manager.current = "confirm_order"

class foodlist(Screen):

	def on_pre_enter(self, *_):
		print("entering foodlist")
		# App.get_running_app().sess.get_values()
		self.get_values()

	def logout(self, *_):
		App.get_running_app().sess.kill()
		self.manager.transition.direction = 'left'
		self.manager.current = 'login'

	def back(self, *_):
		self.manager.current = "menu"

	def get_values(self, *_):
		vendorid =  App.get_running_app().sess.get_vendorid_from_order()
		foodnames = crud.get_food_list(vendorid)
		self.food_list_view.adapter.data = foodnames
		self.food_list_view._trigger_reset_populate()

	def change_to_menu(self, *_):
		self.manager.transition.direction = 'left'
		# modify the current screen to a different "name"
		self.manager.current = 'menu'  


### Confirm order page ###
class confirmOrderScreen(Screen):

	def logout(self, *_):
		App.get_running_app().sess.kill()
		self.manager.transition.direction = 'left'
		self.manager.current = 'login'

	def back(self, *_):
		self.manager.transition.direction = 'left'
		self.manager.current = "foodlist"
		
	def confirm_order(self):
		comments =  self.comments.text
		App.get_running_app().sess.add_comments_to_order(comments)
		App.get_running_app().sess.get_values()
		App.get_running_app().sess.place_order()
		self.manager.current = "order_status"

### orderStatus page ###
class orderListLabel(ListItemLabel):
	pass

class orderStatusScreen(Screen):
	def on_pre_enter(self):
		Clock.schedule_interval(self.refresh, 2) 

	def logout(self, *_):
		App.get_running_app().sess.kill()
		self.manager.transition.direction = 'left'
		self.manager.current = 'login'

	def refresh(self, *_):
		vendorid = App.get_running_app().sess.get_vendorid_from_order()
		pending_and_preparing, ready, late = crud.get_order_status_list(vendorid)
		self.ready_list.adapter.data = ready
		self.ready_list._trigger_reset_populate()
		self.preparing_list.adapter.data = pending_and_preparing
		self.preparing_list._trigger_reset_populate()
		self.late_list.adapter.data = late
		self.late_list._trigger_reset_populate()
		orderid = App.get_running_app().sess.get_orderid()
		self.my_order_number.text = "Your order number is: \n" + str(orderid)

### App ###
class SwitchScreenApp(App):
	sess = session()

	def build(self):
		sm = ScreenManager()
		ms = MenuScreen(name='menu')
		lp = loginpage(name='login')
		fl = foodlist( name='foodlist') 
		sm.add_widget(ms)
		sm.add_widget(lp)
		sm.add_widget(fl)             
		sm.add_widget(confirmOrderScreen(name="confirm_order"))
		sm.add_widget(orderStatusScreen(name="order_status"))
		sm.current = 'login'
		return sm       

if __name__ == '__main__':
	SwitchScreenApp().run()
